export vpy=.venv/bin/python3
export star_wars_dir=star_wars

dev-install:
	python3 -m venv .venv
	$(vpy) -m pip install -r requirements.txt
	$(vpy) $(star_wars_dir)/manage.py migrate
	$(vpy) $(star_wars_dir)/manage.py createsuperuser --email admin@example.com --username admin --no-input

dev-run:
	cd $(star_wars_dir)
	$(vpy) $(star_wars_dir)/manage.py runserver

dev-clean:
	rm -rf .venv
	rm $(star_wars_dir)/db.sqlite3
