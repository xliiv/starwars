# Star Wars Explorer

Explore Star Wars Characters.

# Running

1. Run this command

```shell
make dev-install
# specifyies where the SWAPI app is located
SWAPI_BASE_URL="http://172.17.0.3:8000/" ./star_wars/manage.py runserver
```

2. Visit [http://127.0.0.1:8000/](http://127.0.0.1:8000/)


# Design Decisions

## 1. Class-based views vs REST

Both are pretty cheap using Django Rest Framework (DRF) or Django Class-based Views (CBV)

### DRF

Pros
	exposes REST API for others
Cons
	Views are built with Javascript

### Conclusion

I have picked CBV because
1. although, it is cheap to create views with both DRF and CBV
2. using CBV let you minimize Javascript (JS) code and need of JS frameworks
   which would increase complexity


# Future Work

1. Consider building User interface (UI) using REST API
2. Use ETAG while fetching new data for the People resource
3. Consider using Threading for fetching data for the People reource, since we
   can't go with asyncio (requests lib. is mandatory)
4. Alternatively, resolving Planet's url could be done using indyvidual
   requests for each Planet (instead of current bulk fetch) enhanced by
   [LRU](https://docs.python.org/3/library/functools.html#functools.lru_cache)
5. Get rid of the Petl library or at least fork it & fix ;)
