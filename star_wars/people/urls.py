from django.urls import path

from . import views


app_name = 'people'


urlpatterns = [
    path('dataset', views.DatasetView.as_view(), name='datasets'),
    path('fetch', views.fetch, name='fetch'),
    path('list/<int:id>', views.PersonView.as_view(), name='list'),
    path(
        'value-count/<int:id>',
        views.PersonCountView.as_view(),
        name='value_count',
    ),
]
