import os
import functools
from urllib.parse import urlencode

from django.urls import reverse
from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.views.generic import ListView, TemplateView

from . import models
from .data_providers import swapi


def people_to_csv(url, transform_fn):
    """
    Stores data found at `url` calling `transform_fn` on each item.
    """
    page = swapi.fetch_json(url)
    filename = "{}.csv".format(timezone.now().isoformat())
    csv_path = os.path.join(settings.PEOPLE_CSV_FILE_PATH, filename)
    all_people = swapi.iterate_items(page)
    transformed = map(transform_fn, all_people)
    swapi.items_to_csv(transformed, csv_path)
    return filename, csv_path


class DatasetView(ListView):
    model = models.Dataset
    context_object_name = 'datasets'
    ordering = ['-created']


def fetch(request):
    try:
        homeworld_map = swapi.homeworld_map(settings.SWAPI_PLANET_URL)
        name, csv_path = people_to_csv(
            settings.SWAPI_PEOPLE_URL,
            # TODO: simplifiy it, this partial could be dropped
            functools.partial(
                swapi.transform_person_data, homeworld_map=homeworld_map
            )
        )
    except Exception as e:
        data = {
            'error': "Couldn't fetch the latest data set",
            'object': {},
        }
    else:
        dataset, _ = models.Dataset.objects.get_or_create(
            name=name, file=csv_path,
        )
        data = {
            'error': "",
            'object': {},
        }
    return JsonResponse(data)


class PersonView(TemplateView):
    template_name = 'people/person_list.html'

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)

        start = int(self.request.GET.get('start', 0))
        end = int(self.request.GET.get('end', settings.PEOPLE_PAGE_COUNT))
        end_next = int(end) + settings.PEOPLE_PAGE_COUNT
        dataset = get_object_or_404(models.Dataset, pk=self.kwargs.get('id'))

        if end_next < dataset.table.len():
            kwargs['load_more_url'] = "{}?{}".format(
                reverse('people:list', args=(self.kwargs['id'],)),
                urlencode({'start': start, 'end': end_next}),
            )
        else:
            kwargs['load_more_url'] = None

        dataset.table = dataset.slice(start, end)
        kwargs['dataset'] = dataset
        return kwargs


class PersonCountView(TemplateView):
    template_name = 'people/grouped_people.html'

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        dataset = get_object_or_404(models.Dataset, pk=self.kwargs.get('id'))
        kwargs['group_cols'] = {
            h: h in self.request.GET.getlist('groupCol') for h in dataset.header
        }
        picked_cols = [k for k, v in kwargs['group_cols'].items() if v]
        if picked_cols:
            dataset.table = dataset.aggregate(picked_cols, len)
            dataset.table = dataset.table.rename('value', 'Count')

        kwargs['dataset'] = dataset
        return kwargs
