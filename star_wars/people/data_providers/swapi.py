import csv

import requests
from django.utils.dateparse import parse_datetime


PERSON_KEYS = set((
    "birth_year",
    "eye_color",
    "gender",
    "hair_color",
    "height",
    "homeworld",
    "mass",
    "name",
    "skin_color",
))


def fetch_json(url):
    """Fetches json for `url`."""
    response = requests.get(url)
    response.raise_for_status()
    return response.json()


def iterate_items(page, fetch_page_fn=None) -> dict:
    """Yields all results from page and page's follow ups"""
    fetch_page_fn = fetch_page_fn or fetch_json
    while page:
        for item in page['results']:
            yield item
        next_url = page.get('next', None)
        if next_url:
            page = fetch_page_fn(next_url)
        else:
            page = {}


def transform_items(items, transform_fn):
    for item in items:
        yield transform_fn(item)


def items_to_csv(dict_items, dst):
    """Stores `dict_items` at `dst` as CSV."""
    with open(dst, 'w', newline='') as csvfile:
        item = next(dict_items)
        writer = csv.DictWriter(csvfile, fieldnames=list(item.keys()))
        writer.writeheader()
        writer.writerow(item)

        for item in dict_items:
            writer.writerow(item)


def transform_person_data(data_dict, keys=None, homeworld_map=None):
    """
    NOTE: USES SHALLOW COPY
    """
    homeworld_map = homeworld_map or {}
    keys = keys or PERSON_KEYS
    new_dict = {k: v for k, v in data_dict.items() if k in keys}
    new_dict['date'] = parse_datetime(data_dict['edited']).date()
    homeworld = data_dict['homeworld'] 
    if homeworld in homeworld_map:
        new_dict['homeworld'] = homeworld_map[homeworld]
    return new_dict


def homeworld_map(url):
    """Generates map of `planet url` -> `planet name` based on data found at
    `url`."""
    return {
        p['url']: p['name'] for p in iterate_items(fetch_json(url))
    }
