import copy
import tempfile
from datetime import date

from django.test import TestCase

from people.data_providers import swapi


PERSON_DATA = {
    "name": "Luke Skywalker",
    "height": "172",
    "mass": "77",
    "hair_color": "blond",
    "skin_color": "fair",
    "eye_color": "blue",
    "birth_year": "19BBY",
    "gender": "male",
    "homeworld": "http://172.17.0.3:8000/api/planets/1/",
    "films": [
        "http://172.17.0.3:8000/api/films/1/",
        "http://172.17.0.3:8000/api/films/2/",
        "http://172.17.0.3:8000/api/films/3/",
        "http://172.17.0.3:8000/api/films/6/"
    ],
    "species": [],
    "vehicles": [
        "http://172.17.0.3:8000/api/vehicles/14/",
        "http://172.17.0.3:8000/api/vehicles/30/"
    ],
    "starships": [
        "http://172.17.0.3:8000/api/starships/12/",
        "http://172.17.0.3:8000/api/starships/22/"
    ],
    "created": "2014-12-09T13:50:51.644000Z",
    "edited": "2014-12-20T21:17:56.891000Z",
    "url": "http://172.17.0.3:8000/api/people/1/"
}


class TestIterateItems(TestCase):
    def test_works_for_empty_results(self):
        people = swapi.iterate_items({
            'next': None,
            'results': [],
        })

        self.assertEqual(list(people), [])

    def test_works_if_result_contains_person(self):
        people = swapi.iterate_items({
            'next': None,
            'results': [{
                "name": "Luke Skywalker",
            }],
        })

        self.assertEqual(list(people), [{'name': 'Luke Skywalker'}])

    def test_works_if_next_page_present(self):
        people = swapi.iterate_items({
            "next": "http://172.17.0.3:8000/api/people/?page=2",
            'results': [{
                "name": "Luke Skywalker",
            }],
        }, lambda _: {
            "next": None,
            'results': [{
                "name": "Tion Medon",
            }],
        })

        self.assertEqual(
            list(people),
            [{'name': 'Luke Skywalker'}, {'name': 'Tion Medon'}, ]
        )


class TestItemsToCsv(TestCase):
    def test_file_content_is_correct(self):
        dict_items = iter([{'col1': 1, 'col2': 2}])
        tmp = tempfile.NamedTemporaryFile()
        swapi.items_to_csv(dict_items, tmp.name)

        with open(tmp.name) as hdlr:
            found = hdlr.readlines()
        self.assertEqual(found, ["col1,col2\n", "1,2\n"])


class TestTransformPersonData(TestCase):
    def test_edited_is_renamed_to_date(self):
        person_data = copy.deepcopy(PERSON_DATA)

        found = swapi.transform_person_data(person_data)

        self.assertNotIn('edited', found)
        self.assertEqual(found['date'], date(2014, 12, 20))

    def test_homeworld_is_resolved(self):
        person_data = copy.deepcopy(PERSON_DATA)

        found = swapi.transform_person_data(
            person_data,
            homeworld_map={
                "http://172.17.0.3:8000/api/planets/1/": "Tatooine",
            },
        )

        self.assertEqual(found['homeworld'], "Tatooine")

    def test_use_only_needed_fields(self):
        person_data = copy.deepcopy(PERSON_DATA)

        found = swapi.transform_person_data(person_data)

        self.assertCountEqual(
            found.keys(),
            swapi.PERSON_KEYS.copy() | set(['date']),
        )
