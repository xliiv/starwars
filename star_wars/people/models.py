import petl
from django.db import models


class Dataset(models.Model):
    name = models.CharField(max_length=64)
    file = models.FileField()
    created = models.DateTimeField(auto_now_add=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.table = petl.fromcsv(self.file.name)

    @property
    def header(self):
        # TODO: investigate why we can't get headers without rowslicing O_o
        return self.table.rowslice(0, 0).header()

    @property
    def records(self):
        return self.table.records()

    def slice(self, start, end):
        return self.table.rowslice(start, end)

    def aggregate(self, keys, aggregation_fn):
        # TODO: investigate, why keys can't be a single value list
        keys = keys[0] if len(keys) == 1 else keys
        return self.table.aggregate(key=keys, aggregation=aggregation_fn)
